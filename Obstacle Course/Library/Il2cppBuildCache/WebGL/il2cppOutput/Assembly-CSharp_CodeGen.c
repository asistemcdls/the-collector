﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AriaController::Start()
extern void AriaController_Start_m36CD05730FCDBFDB52D9DE9E7A90D34010F56A4A (void);
// 0x00000002 System.Void AriaController::Update()
extern void AriaController_Update_mA97B08C71417A4B25B874B3DFFF30C7E22B365FB (void);
// 0x00000003 System.Void AriaController::PowerfulIndicatorEmplacement()
extern void AriaController_PowerfulIndicatorEmplacement_mDAC36B985288CFF6B2BB0B06F2D103A9B0294A3C (void);
// 0x00000004 System.Void AriaController::VelocityUpOnPressedKey()
extern void AriaController_VelocityUpOnPressedKey_m1CD40078925F9F4C875851D345A4435C8C13E8A7 (void);
// 0x00000005 System.Void AriaController::Jump()
extern void AriaController_Jump_mCB8EA0368CBED02DC177F45A11D6E491F755148A (void);
// 0x00000006 System.Void AriaController::OnCollisionEnter(UnityEngine.Collision)
extern void AriaController_OnCollisionEnter_mC92748CC983808D6A598D11B877092FAC2E1986E (void);
// 0x00000007 System.Void AriaController::OnTriggerEnter(UnityEngine.Collider)
extern void AriaController_OnTriggerEnter_m718E18C9C10A09F3717CE5100A9705637B1B9019 (void);
// 0x00000008 System.Collections.IEnumerator AriaController::StrikePowerfulRoutine(UnityEngine.GameObject)
extern void AriaController_StrikePowerfulRoutine_m322DE7A43A7A69BA7540311EF2D2EBF7297F16A1 (void);
// 0x00000009 System.Void AriaController::AriaOrientation()
extern void AriaController_AriaOrientation_m292DFBD55A87A5C446874BB81D065268019BD608 (void);
// 0x0000000A System.Void AriaController::.ctor()
extern void AriaController__ctor_m6355512BF8ED66E29476D95F3FAFCC5A98B5BE8F (void);
// 0x0000000B System.Void AriaController/<StrikePowerfulRoutine>d__20::.ctor(System.Int32)
extern void U3CStrikePowerfulRoutineU3Ed__20__ctor_mE1DD61E3763D3FFA831ECD587853B7F9C20367B7 (void);
// 0x0000000C System.Void AriaController/<StrikePowerfulRoutine>d__20::System.IDisposable.Dispose()
extern void U3CStrikePowerfulRoutineU3Ed__20_System_IDisposable_Dispose_mB7FE531E73B359C6C1BE8DD1813D913F653F2BB8 (void);
// 0x0000000D System.Boolean AriaController/<StrikePowerfulRoutine>d__20::MoveNext()
extern void U3CStrikePowerfulRoutineU3Ed__20_MoveNext_m8387F59C6E7EA79E5F15A02A7223E8F0707544FA (void);
// 0x0000000E System.Object AriaController/<StrikePowerfulRoutine>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStrikePowerfulRoutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55DF958238F259DC4F128DADAEF8C37CAFB6C99B (void);
// 0x0000000F System.Void AriaController/<StrikePowerfulRoutine>d__20::System.Collections.IEnumerator.Reset()
extern void U3CStrikePowerfulRoutineU3Ed__20_System_Collections_IEnumerator_Reset_m602C418EE21E57CAD804AEE78979DCDB6E3392B1 (void);
// 0x00000010 System.Object AriaController/<StrikePowerfulRoutine>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CStrikePowerfulRoutineU3Ed__20_System_Collections_IEnumerator_get_Current_m08D9BF97EB0CEA6C73161004B4D0E6F6694694EB (void);
// 0x00000011 System.Void Enemy::Start()
extern void Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02 (void);
// 0x00000012 System.Void Enemy::Update()
extern void Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2 (void);
// 0x00000013 System.Void Enemy::ForwardPlayer()
extern void Enemy_ForwardPlayer_m5FFD6D2BBF09FA6183265EF6ACA67B4E9CB9625A (void);
// 0x00000014 System.Void Enemy::.ctor()
extern void Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734 (void);
// 0x00000015 MainManager MainManager::get_Instance()
extern void MainManager_get_Instance_mC6D937CFA39C866E0D95BF67FE448AE7DBDA1ED0 (void);
// 0x00000016 System.Void MainManager::set_Instance(MainManager)
extern void MainManager_set_Instance_m3038C4A54BABFBC13B37F9C04AE2C5059E7F1434 (void);
// 0x00000017 System.Void MainManager::Awake()
extern void MainManager_Awake_m00A3A3401B125A3CE7EA546FC61FB4D8FD1E0B79 (void);
// 0x00000018 System.Void MainManager::CanvasPanelsInit()
extern void MainManager_CanvasPanelsInit_m978644862D0E0EE48AB1C4B56FB9A212EC00A9B6 (void);
// 0x00000019 System.Void MainManager::Start()
extern void MainManager_Start_mC9214444CF64A4908FDF87B213F21EB4372ABAB7 (void);
// 0x0000001A System.Void MainManager::Update()
extern void MainManager_Update_m2D4BE865BE5198840B173ADDC4C5E8F647D88D46 (void);
// 0x0000001B System.Void MainManager::InitTeleporter()
extern void MainManager_InitTeleporter_mD3D29DA09AD7F82E44C6105CC6C7CA6A8053D514 (void);
// 0x0000001C System.Void MainManager::InitTextComposant()
extern void MainManager_InitTextComposant_m3470BD68BA07A593FC7A9CC1582E7F3ECF41D84D (void);
// 0x0000001D System.Void MainManager::IntroInfos()
extern void MainManager_IntroInfos_m5BACDECA99EC79331BC8DBA86FE5B249B262C5A7 (void);
// 0x0000001E System.Void MainManager::PlayAudioClip(UnityEngine.AudioClip,System.Boolean)
extern void MainManager_PlayAudioClip_m8B7CD4345233D9EB1C520E0B35B69F5B325051D2 (void);
// 0x0000001F System.Boolean MainManager::CanPlay()
extern void MainManager_CanPlay_mE7B024DB7C7918D16F665182F16D2264105EA11F (void);
// 0x00000020 System.Void MainManager::CanvasPanelsManagement()
extern void MainManager_CanvasPanelsManagement_m8F634FB97ACCC07C1927280C98B682F56CB69DC3 (void);
// 0x00000021 UnityEngine.RectTransform MainManager::FindContent(UnityEngine.GameObject,System.String)
extern void MainManager_FindContent_m89BB61D818E34DECE707C2FF033C082FDCAF84A3 (void);
// 0x00000022 System.Void MainManager::GoToMenu()
extern void MainManager_GoToMenu_m74CD430547B49523B240A74A6EF7073C08C7B0D8 (void);
// 0x00000023 System.Void MainManager::GoToMission()
extern void MainManager_GoToMission_mD225C9FD69C1C75FAE6D337BFC72FEBE39974178 (void);
// 0x00000024 System.Void MainManager::InstantiateTeleporter()
extern void MainManager_InstantiateTeleporter_mBBA7920A4C09F3F7F4FB6E13C791C7A7DCA2E330 (void);
// 0x00000025 System.Void MainManager::InstantiateAlertText(System.String,System.Int32)
extern void MainManager_InstantiateAlertText_mEE5F9C8EF437C016C973F01676F9F5EA11C2A32D (void);
// 0x00000026 System.Void MainManager::UpdateItemCountText()
extern void MainManager_UpdateItemCountText_mF147611055CCCE6AFEF7D442B0F243EE9CBBF74C (void);
// 0x00000027 System.Void MainManager::NextLevel()
extern void MainManager_NextLevel_m9D1AE0230EEF3AC30CB50545BE5B7E0316423EAE (void);
// 0x00000028 System.Void MainManager::RestartGame()
extern void MainManager_RestartGame_m86B477C70FEC051F2E928BA25CF4C0AF56953FC1 (void);
// 0x00000029 System.Void MainManager::GameStart()
extern void MainManager_GameStart_m6E447BA01089CD238F4B359E5A538B0B12F3CB9A (void);
// 0x0000002A System.Void MainManager::GameOver()
extern void MainManager_GameOver_mF25DFCA9A41A2E217FDF1AE5D81155C7DAB19FC3 (void);
// 0x0000002B System.Collections.IEnumerator MainManager::AlertCoroutine(System.Single)
extern void MainManager_AlertCoroutine_mE167531481872836D048BFD852C6D1AC3B97B158 (void);
// 0x0000002C System.Collections.IEnumerator MainManager::AlertTextCoroutine(UnityEngine.GameObject,System.Single)
extern void MainManager_AlertTextCoroutine_m684570A9202890BFCAC29D9E33DFEEBC96F090DC (void);
// 0x0000002D System.Collections.IEnumerator MainManager::StartCountdown(System.Single)
extern void MainManager_StartCountdown_m2E742EAC78B36D8BE0117132B3DB64ED21A6C62B (void);
// 0x0000002E System.Collections.IEnumerator MainManager::GamePartCountdown(System.Single)
extern void MainManager_GamePartCountdown_mA0E98A980ADDDCC380A9EA635645A94F93A9E272 (void);
// 0x0000002F System.Collections.IEnumerator MainManager::GameMusicCountdown()
extern void MainManager_GameMusicCountdown_m2BF2FA05CB60EC0D09E82D65BBD3BFA7A0DFEBF0 (void);
// 0x00000030 System.Void MainManager::Exit()
extern void MainManager_Exit_mE8D013CA0D151A859A98C2E36DD48F69E04EC56F (void);
// 0x00000031 System.Void MainManager::SaveData()
extern void MainManager_SaveData_m326E77D35620882FF6C5B3E66670326A2C47C173 (void);
// 0x00000032 System.Void MainManager::LoadDatas()
extern void MainManager_LoadDatas_m04FBD8CA44AE7554A367BBBD366F0C1C261905FB (void);
// 0x00000033 System.Void MainManager::.ctor()
extern void MainManager__ctor_m90DF2FBE6170D98DF8F9C03C44FB914322C4D10F (void);
// 0x00000034 System.Void MainManager/<AlertCoroutine>d__68::.ctor(System.Int32)
extern void U3CAlertCoroutineU3Ed__68__ctor_m85E09A06C48A513AEB8DDA3FAC7DF7EC7931DA5C (void);
// 0x00000035 System.Void MainManager/<AlertCoroutine>d__68::System.IDisposable.Dispose()
extern void U3CAlertCoroutineU3Ed__68_System_IDisposable_Dispose_m0C6C28147F328D4AE901F82CE71AA6D2884FAC29 (void);
// 0x00000036 System.Boolean MainManager/<AlertCoroutine>d__68::MoveNext()
extern void U3CAlertCoroutineU3Ed__68_MoveNext_m1DD13B8E182723873B4AADD91FFAEB662F21A925 (void);
// 0x00000037 System.Object MainManager/<AlertCoroutine>d__68::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAlertCoroutineU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF67F3B8E608BED312811549E7D631CDDEB441673 (void);
// 0x00000038 System.Void MainManager/<AlertCoroutine>d__68::System.Collections.IEnumerator.Reset()
extern void U3CAlertCoroutineU3Ed__68_System_Collections_IEnumerator_Reset_m0C46910B5D6C041127E5609F2C403B4B53A1A7F6 (void);
// 0x00000039 System.Object MainManager/<AlertCoroutine>d__68::System.Collections.IEnumerator.get_Current()
extern void U3CAlertCoroutineU3Ed__68_System_Collections_IEnumerator_get_Current_m5C08995B46BA80DFEF8AB3962C8799B2447089FF (void);
// 0x0000003A System.Void MainManager/<AlertTextCoroutine>d__69::.ctor(System.Int32)
extern void U3CAlertTextCoroutineU3Ed__69__ctor_m2762E37A95044E43FEB1C09E7CB91BB9E27A8306 (void);
// 0x0000003B System.Void MainManager/<AlertTextCoroutine>d__69::System.IDisposable.Dispose()
extern void U3CAlertTextCoroutineU3Ed__69_System_IDisposable_Dispose_mB8BAE389C123713F32DBD320B74CA62456C9E8A5 (void);
// 0x0000003C System.Boolean MainManager/<AlertTextCoroutine>d__69::MoveNext()
extern void U3CAlertTextCoroutineU3Ed__69_MoveNext_mF372416ABCAEA47A1981CC3A49B12568E4F76860 (void);
// 0x0000003D System.Object MainManager/<AlertTextCoroutine>d__69::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAlertTextCoroutineU3Ed__69_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA2F58847DE553000B269D7B2A2992C27456A14F (void);
// 0x0000003E System.Void MainManager/<AlertTextCoroutine>d__69::System.Collections.IEnumerator.Reset()
extern void U3CAlertTextCoroutineU3Ed__69_System_Collections_IEnumerator_Reset_m69958B75D7F19B2B10C37CCF5F2CB33CECC26286 (void);
// 0x0000003F System.Object MainManager/<AlertTextCoroutine>d__69::System.Collections.IEnumerator.get_Current()
extern void U3CAlertTextCoroutineU3Ed__69_System_Collections_IEnumerator_get_Current_m9FDA22F65F03BC9ECF12FFD8DA88576453D283B1 (void);
// 0x00000040 System.Void MainManager/<StartCountdown>d__70::.ctor(System.Int32)
extern void U3CStartCountdownU3Ed__70__ctor_m367FC7246F0A8A8EBD580B2D35A50D49B1D65B55 (void);
// 0x00000041 System.Void MainManager/<StartCountdown>d__70::System.IDisposable.Dispose()
extern void U3CStartCountdownU3Ed__70_System_IDisposable_Dispose_m525E0F199F8FCE791100D5B29F63DA7E034A02B7 (void);
// 0x00000042 System.Boolean MainManager/<StartCountdown>d__70::MoveNext()
extern void U3CStartCountdownU3Ed__70_MoveNext_m84BFDD227B28BCB55D705D9B35DFC7FEECF6EF49 (void);
// 0x00000043 System.Object MainManager/<StartCountdown>d__70::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartCountdownU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0452381F1245C64E3BC1F586A52457CAE56B8E3A (void);
// 0x00000044 System.Void MainManager/<StartCountdown>d__70::System.Collections.IEnumerator.Reset()
extern void U3CStartCountdownU3Ed__70_System_Collections_IEnumerator_Reset_mAC68B4ABA7F8037A686286337FB0D185AABFEFA5 (void);
// 0x00000045 System.Object MainManager/<StartCountdown>d__70::System.Collections.IEnumerator.get_Current()
extern void U3CStartCountdownU3Ed__70_System_Collections_IEnumerator_get_Current_m6CC8A9E2DD97E1C6E8E1EB06E68673F0A24501CB (void);
// 0x00000046 System.Void MainManager/<GamePartCountdown>d__71::.ctor(System.Int32)
extern void U3CGamePartCountdownU3Ed__71__ctor_m60F06D90E7C377482DC4769C1F5AAF9C372A3DCD (void);
// 0x00000047 System.Void MainManager/<GamePartCountdown>d__71::System.IDisposable.Dispose()
extern void U3CGamePartCountdownU3Ed__71_System_IDisposable_Dispose_mB471C1715C9CCFDE54C7C84874948AC57887DBFC (void);
// 0x00000048 System.Boolean MainManager/<GamePartCountdown>d__71::MoveNext()
extern void U3CGamePartCountdownU3Ed__71_MoveNext_m454CF782A7E30EA04BB7A2F631AC4ABBB0E78006 (void);
// 0x00000049 System.Object MainManager/<GamePartCountdown>d__71::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGamePartCountdownU3Ed__71_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6A357E16FDA93D4AB5CF3E6B53CC83877C8C8D8 (void);
// 0x0000004A System.Void MainManager/<GamePartCountdown>d__71::System.Collections.IEnumerator.Reset()
extern void U3CGamePartCountdownU3Ed__71_System_Collections_IEnumerator_Reset_mDAE9FCDCD16F792FB2ACACB51C36B989C887D981 (void);
// 0x0000004B System.Object MainManager/<GamePartCountdown>d__71::System.Collections.IEnumerator.get_Current()
extern void U3CGamePartCountdownU3Ed__71_System_Collections_IEnumerator_get_Current_m833900B32FA3AC23251BA1177740F688C8AB1DDB (void);
// 0x0000004C System.Void MainManager/<GameMusicCountdown>d__72::.ctor(System.Int32)
extern void U3CGameMusicCountdownU3Ed__72__ctor_m7D66FBB0DA0CFB5E9D2F976F79EF0D2AC9FBBABC (void);
// 0x0000004D System.Void MainManager/<GameMusicCountdown>d__72::System.IDisposable.Dispose()
extern void U3CGameMusicCountdownU3Ed__72_System_IDisposable_Dispose_m58D0A5E4F8546B1049EB6D786C6D5EF2BE7D328A (void);
// 0x0000004E System.Boolean MainManager/<GameMusicCountdown>d__72::MoveNext()
extern void U3CGameMusicCountdownU3Ed__72_MoveNext_m47B7B16BF3653A466F9241059FCC5969644205C6 (void);
// 0x0000004F System.Object MainManager/<GameMusicCountdown>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGameMusicCountdownU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30975FDCDAE774654F50D1952CCC5320A3EB4E2D (void);
// 0x00000050 System.Void MainManager/<GameMusicCountdown>d__72::System.Collections.IEnumerator.Reset()
extern void U3CGameMusicCountdownU3Ed__72_System_Collections_IEnumerator_Reset_m7C8A28564D78FC7FE968428CFE0393FB23DE0845 (void);
// 0x00000051 System.Object MainManager/<GameMusicCountdown>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CGameMusicCountdownU3Ed__72_System_Collections_IEnumerator_get_Current_m5036D88BB77F55F15BA1D6487D43C5BC3894634C (void);
// 0x00000052 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x00000053 System.Void RotateCamera::Start()
extern void RotateCamera_Start_m07040E208272F59CC87DBAFA82911BA3B3A0FA7B (void);
// 0x00000054 System.Void RotateCamera::Update()
extern void RotateCamera_Update_mC8F73FD5F0C2F2353E383276BE3A82D15A1BE376 (void);
// 0x00000055 System.Void RotateCamera::RotateWithAxis()
extern void RotateCamera_RotateWithAxis_m4CF080606013CBAAA591828E882D05E25667340C (void);
// 0x00000056 System.Void RotateCamera::RotateAlways()
extern void RotateCamera_RotateAlways_m0A39ADFC3F3F81BE62C9EC24EEA9F6DD22BA43F7 (void);
// 0x00000057 System.Void RotateCamera::.ctor()
extern void RotateCamera__ctor_m08D220CA79A2FC6634119A6C982435EBCD0B0E5E (void);
// 0x00000058 System.Void SpinController::Start()
extern void SpinController_Start_mD19D60DB580760540EDA9454E3A541D105377715 (void);
// 0x00000059 System.Void SpinController::Update()
extern void SpinController_Update_m463F6F92AD8A6F3573A97E9DC8155C6056DC8D48 (void);
// 0x0000005A System.Void SpinController::RotateObj(UnityEngine.GameObject,UnityEngine.Quaternion)
extern void SpinController_RotateObj_mE82A581164FD8FF449A83CA05B5C92948CC45046 (void);
// 0x0000005B System.Void SpinController::.ctor()
extern void SpinController__ctor_m9104A92BBA0183CDB1E4C5D6E545FBC3EBDE1CD9 (void);
// 0x0000005C LevelsComposants LevelsComposants::get_Instance()
extern void LevelsComposants_get_Instance_m701026AD08BA0852DD4C8ED2D3B8D984E019A222 (void);
// 0x0000005D System.Void LevelsComposants::set_Instance(LevelsComposants)
extern void LevelsComposants_set_Instance_m6A2FCA47B4F1E04192735DA6CBFD5E22B65CAD20 (void);
// 0x0000005E System.Void LevelsComposants::Awake()
extern void LevelsComposants_Awake_m362F0F59A955C86DE9D044104CCC03B0C4B6BB7F (void);
// 0x0000005F System.Int32 LevelsComposants::get_level()
extern void LevelsComposants_get_level_m74AA3CB641E360DE45F58B8290AD7CD81279669F (void);
// 0x00000060 System.Void LevelsComposants::set_level(System.Int32)
extern void LevelsComposants_set_level_m74CAD1C6087DDCA9A19ADD1B3F53C243ED915A0C (void);
// 0x00000061 System.Void LevelsComposants::UpdateLevel(System.Int32)
extern void LevelsComposants_UpdateLevel_mAD34C02F544428A7F6CB74D72C8D81E9737FA8CA (void);
// 0x00000062 System.Single LevelsComposants::GetLevelTimeCount(System.Int32)
extern void LevelsComposants_GetLevelTimeCount_m3F696C372104BFC3727C3303981EC3B1A326E84C (void);
// 0x00000063 System.Int32 LevelsComposants::GetLevelItemCount(System.Int32)
extern void LevelsComposants_GetLevelItemCount_mAF8564A7220B9CB72ACF7B94CB675D5438B44EFF (void);
// 0x00000064 System.Void LevelsComposants::GetLevelScene(System.Int32)
extern void LevelsComposants_GetLevelScene_mA76D0016C988D149853A3898C37AFC96DE862C63 (void);
// 0x00000065 System.Collections.Generic.List`1<System.String> LevelsComposants::GetLevelMissions(System.Int32,System.Int32,System.Single)
extern void LevelsComposants_GetLevelMissions_mC1DAB7F5A7A5C5FEE4B8D3850D88E48CFDE4314B (void);
// 0x00000066 System.Collections.Generic.List`1<System.String> LevelsComposants::GetInfos()
extern void LevelsComposants_GetInfos_m002C16BD7EE1A24910ADD9560A32A7020D19BADC (void);
// 0x00000067 System.Void LevelsComposants::.ctor()
extern void LevelsComposants__ctor_mA97718810490EBD066F759104481A045FF5039FC (void);
// 0x00000068 Tags Tags::get_Instance()
extern void Tags_get_Instance_mDAC864FD56CC26C950485B66C1862A6951256389 (void);
// 0x00000069 System.Void Tags::set_Instance(Tags)
extern void Tags_set_Instance_mB84343F16E13F904F722C898791372189EAB273F (void);
// 0x0000006A System.Void Tags::Awake()
extern void Tags_Awake_mA9A9E41307FEA3823038BD94E84185D586172D1A (void);
// 0x0000006B System.Void Tags::.ctor()
extern void Tags__ctor_m0A1D6BCDC70E2356528170DCD610BCEC520922FC (void);
// 0x0000006C System.Void Readme::.ctor()
extern void Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448 (void);
// 0x0000006D System.Void Readme/Section::.ctor()
extern void Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E (void);
// 0x0000006E System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C (void);
// 0x0000006F UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A (void);
// 0x00000070 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A (void);
// 0x00000071 System.Single UnityTemplateProjects.SimpleCameraController::GetBoostFactor()
extern void SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE (void);
// 0x00000072 UnityEngine.Vector2 UnityTemplateProjects.SimpleCameraController::GetInputLookRotation()
extern void SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9 (void);
// 0x00000073 System.Boolean UnityTemplateProjects.SimpleCameraController::IsBoostPressed()
extern void SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0 (void);
// 0x00000074 System.Boolean UnityTemplateProjects.SimpleCameraController::IsEscapePressed()
extern void SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D (void);
// 0x00000075 System.Boolean UnityTemplateProjects.SimpleCameraController::IsCameraRotationAllowed()
extern void SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA (void);
// 0x00000076 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonDown()
extern void SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A (void);
// 0x00000077 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonUp()
extern void SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5 (void);
// 0x00000078 System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87 (void);
// 0x00000079 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647 (void);
// 0x0000007A System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96 (void);
// 0x0000007B System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326 (void);
// 0x0000007C System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410 (void);
// 0x0000007D System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D (void);
static Il2CppMethodPointer s_methodPointers[125] = 
{
	AriaController_Start_m36CD05730FCDBFDB52D9DE9E7A90D34010F56A4A,
	AriaController_Update_mA97B08C71417A4B25B874B3DFFF30C7E22B365FB,
	AriaController_PowerfulIndicatorEmplacement_mDAC36B985288CFF6B2BB0B06F2D103A9B0294A3C,
	AriaController_VelocityUpOnPressedKey_m1CD40078925F9F4C875851D345A4435C8C13E8A7,
	AriaController_Jump_mCB8EA0368CBED02DC177F45A11D6E491F755148A,
	AriaController_OnCollisionEnter_mC92748CC983808D6A598D11B877092FAC2E1986E,
	AriaController_OnTriggerEnter_m718E18C9C10A09F3717CE5100A9705637B1B9019,
	AriaController_StrikePowerfulRoutine_m322DE7A43A7A69BA7540311EF2D2EBF7297F16A1,
	AriaController_AriaOrientation_m292DFBD55A87A5C446874BB81D065268019BD608,
	AriaController__ctor_m6355512BF8ED66E29476D95F3FAFCC5A98B5BE8F,
	U3CStrikePowerfulRoutineU3Ed__20__ctor_mE1DD61E3763D3FFA831ECD587853B7F9C20367B7,
	U3CStrikePowerfulRoutineU3Ed__20_System_IDisposable_Dispose_mB7FE531E73B359C6C1BE8DD1813D913F653F2BB8,
	U3CStrikePowerfulRoutineU3Ed__20_MoveNext_m8387F59C6E7EA79E5F15A02A7223E8F0707544FA,
	U3CStrikePowerfulRoutineU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m55DF958238F259DC4F128DADAEF8C37CAFB6C99B,
	U3CStrikePowerfulRoutineU3Ed__20_System_Collections_IEnumerator_Reset_m602C418EE21E57CAD804AEE78979DCDB6E3392B1,
	U3CStrikePowerfulRoutineU3Ed__20_System_Collections_IEnumerator_get_Current_m08D9BF97EB0CEA6C73161004B4D0E6F6694694EB,
	Enemy_Start_m9FA35B427F2B9FDFD390E9812C2556775C62CB02,
	Enemy_Update_mA01EE7AF5D3B97687752E9D22BECB4A3E13F8FD2,
	Enemy_ForwardPlayer_m5FFD6D2BBF09FA6183265EF6ACA67B4E9CB9625A,
	Enemy__ctor_m3C82F8269DE4132408E15B523907244771640734,
	MainManager_get_Instance_mC6D937CFA39C866E0D95BF67FE448AE7DBDA1ED0,
	MainManager_set_Instance_m3038C4A54BABFBC13B37F9C04AE2C5059E7F1434,
	MainManager_Awake_m00A3A3401B125A3CE7EA546FC61FB4D8FD1E0B79,
	MainManager_CanvasPanelsInit_m978644862D0E0EE48AB1C4B56FB9A212EC00A9B6,
	MainManager_Start_mC9214444CF64A4908FDF87B213F21EB4372ABAB7,
	MainManager_Update_m2D4BE865BE5198840B173ADDC4C5E8F647D88D46,
	MainManager_InitTeleporter_mD3D29DA09AD7F82E44C6105CC6C7CA6A8053D514,
	MainManager_InitTextComposant_m3470BD68BA07A593FC7A9CC1582E7F3ECF41D84D,
	MainManager_IntroInfos_m5BACDECA99EC79331BC8DBA86FE5B249B262C5A7,
	MainManager_PlayAudioClip_m8B7CD4345233D9EB1C520E0B35B69F5B325051D2,
	MainManager_CanPlay_mE7B024DB7C7918D16F665182F16D2264105EA11F,
	MainManager_CanvasPanelsManagement_m8F634FB97ACCC07C1927280C98B682F56CB69DC3,
	MainManager_FindContent_m89BB61D818E34DECE707C2FF033C082FDCAF84A3,
	MainManager_GoToMenu_m74CD430547B49523B240A74A6EF7073C08C7B0D8,
	MainManager_GoToMission_mD225C9FD69C1C75FAE6D337BFC72FEBE39974178,
	MainManager_InstantiateTeleporter_mBBA7920A4C09F3F7F4FB6E13C791C7A7DCA2E330,
	MainManager_InstantiateAlertText_mEE5F9C8EF437C016C973F01676F9F5EA11C2A32D,
	MainManager_UpdateItemCountText_mF147611055CCCE6AFEF7D442B0F243EE9CBBF74C,
	MainManager_NextLevel_m9D1AE0230EEF3AC30CB50545BE5B7E0316423EAE,
	MainManager_RestartGame_m86B477C70FEC051F2E928BA25CF4C0AF56953FC1,
	MainManager_GameStart_m6E447BA01089CD238F4B359E5A538B0B12F3CB9A,
	MainManager_GameOver_mF25DFCA9A41A2E217FDF1AE5D81155C7DAB19FC3,
	MainManager_AlertCoroutine_mE167531481872836D048BFD852C6D1AC3B97B158,
	MainManager_AlertTextCoroutine_m684570A9202890BFCAC29D9E33DFEEBC96F090DC,
	MainManager_StartCountdown_m2E742EAC78B36D8BE0117132B3DB64ED21A6C62B,
	MainManager_GamePartCountdown_mA0E98A980ADDDCC380A9EA635645A94F93A9E272,
	MainManager_GameMusicCountdown_m2BF2FA05CB60EC0D09E82D65BBD3BFA7A0DFEBF0,
	MainManager_Exit_mE8D013CA0D151A859A98C2E36DD48F69E04EC56F,
	MainManager_SaveData_m326E77D35620882FF6C5B3E66670326A2C47C173,
	MainManager_LoadDatas_m04FBD8CA44AE7554A367BBBD366F0C1C261905FB,
	MainManager__ctor_m90DF2FBE6170D98DF8F9C03C44FB914322C4D10F,
	U3CAlertCoroutineU3Ed__68__ctor_m85E09A06C48A513AEB8DDA3FAC7DF7EC7931DA5C,
	U3CAlertCoroutineU3Ed__68_System_IDisposable_Dispose_m0C6C28147F328D4AE901F82CE71AA6D2884FAC29,
	U3CAlertCoroutineU3Ed__68_MoveNext_m1DD13B8E182723873B4AADD91FFAEB662F21A925,
	U3CAlertCoroutineU3Ed__68_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF67F3B8E608BED312811549E7D631CDDEB441673,
	U3CAlertCoroutineU3Ed__68_System_Collections_IEnumerator_Reset_m0C46910B5D6C041127E5609F2C403B4B53A1A7F6,
	U3CAlertCoroutineU3Ed__68_System_Collections_IEnumerator_get_Current_m5C08995B46BA80DFEF8AB3962C8799B2447089FF,
	U3CAlertTextCoroutineU3Ed__69__ctor_m2762E37A95044E43FEB1C09E7CB91BB9E27A8306,
	U3CAlertTextCoroutineU3Ed__69_System_IDisposable_Dispose_mB8BAE389C123713F32DBD320B74CA62456C9E8A5,
	U3CAlertTextCoroutineU3Ed__69_MoveNext_mF372416ABCAEA47A1981CC3A49B12568E4F76860,
	U3CAlertTextCoroutineU3Ed__69_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFA2F58847DE553000B269D7B2A2992C27456A14F,
	U3CAlertTextCoroutineU3Ed__69_System_Collections_IEnumerator_Reset_m69958B75D7F19B2B10C37CCF5F2CB33CECC26286,
	U3CAlertTextCoroutineU3Ed__69_System_Collections_IEnumerator_get_Current_m9FDA22F65F03BC9ECF12FFD8DA88576453D283B1,
	U3CStartCountdownU3Ed__70__ctor_m367FC7246F0A8A8EBD580B2D35A50D49B1D65B55,
	U3CStartCountdownU3Ed__70_System_IDisposable_Dispose_m525E0F199F8FCE791100D5B29F63DA7E034A02B7,
	U3CStartCountdownU3Ed__70_MoveNext_m84BFDD227B28BCB55D705D9B35DFC7FEECF6EF49,
	U3CStartCountdownU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0452381F1245C64E3BC1F586A52457CAE56B8E3A,
	U3CStartCountdownU3Ed__70_System_Collections_IEnumerator_Reset_mAC68B4ABA7F8037A686286337FB0D185AABFEFA5,
	U3CStartCountdownU3Ed__70_System_Collections_IEnumerator_get_Current_m6CC8A9E2DD97E1C6E8E1EB06E68673F0A24501CB,
	U3CGamePartCountdownU3Ed__71__ctor_m60F06D90E7C377482DC4769C1F5AAF9C372A3DCD,
	U3CGamePartCountdownU3Ed__71_System_IDisposable_Dispose_mB471C1715C9CCFDE54C7C84874948AC57887DBFC,
	U3CGamePartCountdownU3Ed__71_MoveNext_m454CF782A7E30EA04BB7A2F631AC4ABBB0E78006,
	U3CGamePartCountdownU3Ed__71_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF6A357E16FDA93D4AB5CF3E6B53CC83877C8C8D8,
	U3CGamePartCountdownU3Ed__71_System_Collections_IEnumerator_Reset_mDAE9FCDCD16F792FB2ACACB51C36B989C887D981,
	U3CGamePartCountdownU3Ed__71_System_Collections_IEnumerator_get_Current_m833900B32FA3AC23251BA1177740F688C8AB1DDB,
	U3CGameMusicCountdownU3Ed__72__ctor_m7D66FBB0DA0CFB5E9D2F976F79EF0D2AC9FBBABC,
	U3CGameMusicCountdownU3Ed__72_System_IDisposable_Dispose_m58D0A5E4F8546B1049EB6D786C6D5EF2BE7D328A,
	U3CGameMusicCountdownU3Ed__72_MoveNext_m47B7B16BF3653A466F9241059FCC5969644205C6,
	U3CGameMusicCountdownU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30975FDCDAE774654F50D1952CCC5320A3EB4E2D,
	U3CGameMusicCountdownU3Ed__72_System_Collections_IEnumerator_Reset_m7C8A28564D78FC7FE968428CFE0393FB23DE0845,
	U3CGameMusicCountdownU3Ed__72_System_Collections_IEnumerator_get_Current_m5036D88BB77F55F15BA1D6487D43C5BC3894634C,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	RotateCamera_Start_m07040E208272F59CC87DBAFA82911BA3B3A0FA7B,
	RotateCamera_Update_mC8F73FD5F0C2F2353E383276BE3A82D15A1BE376,
	RotateCamera_RotateWithAxis_m4CF080606013CBAAA591828E882D05E25667340C,
	RotateCamera_RotateAlways_m0A39ADFC3F3F81BE62C9EC24EEA9F6DD22BA43F7,
	RotateCamera__ctor_m08D220CA79A2FC6634119A6C982435EBCD0B0E5E,
	SpinController_Start_mD19D60DB580760540EDA9454E3A541D105377715,
	SpinController_Update_m463F6F92AD8A6F3573A97E9DC8155C6056DC8D48,
	SpinController_RotateObj_mE82A581164FD8FF449A83CA05B5C92948CC45046,
	SpinController__ctor_m9104A92BBA0183CDB1E4C5D6E545FBC3EBDE1CD9,
	LevelsComposants_get_Instance_m701026AD08BA0852DD4C8ED2D3B8D984E019A222,
	LevelsComposants_set_Instance_m6A2FCA47B4F1E04192735DA6CBFD5E22B65CAD20,
	LevelsComposants_Awake_m362F0F59A955C86DE9D044104CCC03B0C4B6BB7F,
	LevelsComposants_get_level_m74AA3CB641E360DE45F58B8290AD7CD81279669F,
	LevelsComposants_set_level_m74CAD1C6087DDCA9A19ADD1B3F53C243ED915A0C,
	LevelsComposants_UpdateLevel_mAD34C02F544428A7F6CB74D72C8D81E9737FA8CA,
	LevelsComposants_GetLevelTimeCount_m3F696C372104BFC3727C3303981EC3B1A326E84C,
	LevelsComposants_GetLevelItemCount_mAF8564A7220B9CB72ACF7B94CB675D5438B44EFF,
	LevelsComposants_GetLevelScene_mA76D0016C988D149853A3898C37AFC96DE862C63,
	LevelsComposants_GetLevelMissions_mC1DAB7F5A7A5C5FEE4B8D3850D88E48CFDE4314B,
	LevelsComposants_GetInfos_m002C16BD7EE1A24910ADD9560A32A7020D19BADC,
	LevelsComposants__ctor_mA97718810490EBD066F759104481A045FF5039FC,
	Tags_get_Instance_mDAC864FD56CC26C950485B66C1862A6951256389,
	Tags_set_Instance_mB84343F16E13F904F722C898791372189EAB273F,
	Tags_Awake_mA9A9E41307FEA3823038BD94E84185D586172D1A,
	Tags__ctor_m0A1D6BCDC70E2356528170DCD610BCEC520922FC,
	Readme__ctor_mF465410C5B2E598F2685E82CFCE1F42186AFF448,
	Section__ctor_mBAD5262A353BC071C61B8DB462A3D4D5AB5C7C4E,
	SimpleCameraController_OnEnable_m2BA1F31BEDE84695933E86CF731059D6FAC2111C,
	SimpleCameraController_GetInputTranslationDirection_m924AB4CEA66ADC4F1C63EAC21B660E0495765F4A,
	SimpleCameraController_Update_m0E587F33074BE72C54BA835B2217B0E8EC94F24A,
	SimpleCameraController_GetBoostFactor_m42B667ADEACC6F9CF0E4B6549A501609A15716AE,
	SimpleCameraController_GetInputLookRotation_m4F945850C9036FAF5C3436AA42A1C3CB918E1BB9,
	SimpleCameraController_IsBoostPressed_m26120B18155EBB68597EA52F392AD1E59F9A71E0,
	SimpleCameraController_IsEscapePressed_mC694179281D2F4B20E831995751C70A26806488D,
	SimpleCameraController_IsCameraRotationAllowed_mABB9E26C1CC5C5643B305685F046B4B8493C7AFA,
	SimpleCameraController_IsRightMouseButtonDown_m0DE4F75C0A0AE963F333712F226000E0AE2DC32A,
	SimpleCameraController_IsRightMouseButtonUp_mEDE93C76DE7120043B15F390865340D1C5165CE5,
	SimpleCameraController__ctor_mC6AE2509DDF461856450EC7DE0058A1687AB5C87,
	CameraState_SetFromTransform_mAF13C515CFB1085295C01A870D93375E98F16647,
	CameraState_Translate_mB8F7239BD9DB70190E59D47D75DD125AD9AF3A96,
	CameraState_LerpTowards_mF2D4B962A677B281ED2F539A2FFF8A693FB9A326,
	CameraState_UpdateTransform_mE653356FD34828D19ECB6793439A14C38F372410,
	CameraState__ctor_m9C5338CABE70B8C73F8A4A08C1AFA1B33417DE9D,
};
static const int32_t s_InvokerIndices[125] = 
{
	3334,
	3334,
	3334,
	3334,
	3334,
	2737,
	2737,
	2106,
	3334,
	3334,
	2720,
	3334,
	3298,
	3269,
	3334,
	3269,
	3334,
	3334,
	3334,
	3334,
	5016,
	4973,
	3334,
	3334,
	3334,
	3334,
	3334,
	3334,
	3334,
	1576,
	3298,
	3334,
	1113,
	3334,
	3334,
	3334,
	1566,
	3334,
	3334,
	3334,
	3334,
	3334,
	2110,
	1115,
	2110,
	2110,
	3269,
	3334,
	3334,
	3334,
	3334,
	2720,
	3334,
	3298,
	3269,
	3334,
	3269,
	2720,
	3334,
	3298,
	3269,
	3334,
	3269,
	2720,
	3334,
	3298,
	3269,
	3334,
	3269,
	2720,
	3334,
	3298,
	3269,
	3334,
	3269,
	2720,
	3334,
	3298,
	3269,
	3334,
	3269,
	3334,
	3334,
	3334,
	3334,
	3334,
	3334,
	3334,
	3334,
	1572,
	3334,
	5016,
	4973,
	3334,
	3251,
	2720,
	2720,
	2497,
	1969,
	2720,
	747,
	3269,
	3334,
	5016,
	4973,
	3334,
	3334,
	3334,
	3334,
	3334,
	3329,
	3334,
	3306,
	3327,
	3298,
	3298,
	3298,
	3298,
	3298,
	3334,
	2737,
	2798,
	941,
	2737,
	3334,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	125,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
