using System.Collections;
using UnityEngine;


public class AriaController : MonoBehaviour
{
    private GameObject focalPoint;
    public GameObject strikePowerfulIndicator;
    public GameObject enemyPrefab;
    private Rigidbody ariaRb;
    private float horizontalInput = 0.0f;
    private float verticalInput = 0.0f; 
    [SerializeField] float speedForce = 500.0f;
    [SerializeField] float jumpForce = 5.0f;
    [SerializeField] float strikePowerfulFactor = 25.0f;
    private float velocityOnOrderForce = 3000.0f;
    private bool hasStrikePowerful = false;
    private int strikePowerfulTime = 7;
    private GameObject ground;


    // Start is called before the first frame update
    void Start()
    {
        // Focal point
        focalPoint = GameObject.FindGameObjectWithTag(Tags.Instance.focalPoint);
        // Ground
        ground = GameObject.FindGameObjectWithTag(Tags.Instance.ground);
        // Rigid body
        ariaRb = GetComponent<Rigidbody>();
        strikePowerfulIndicator.SetActive(false); 
    }

    // Update is called once per frame
    void Update()
    {
        // Get axis throught keyboard directions
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");
        
        if(MainManager.Instance.CanPlay()) {
            // Update player orientation
            AriaOrientation();
            // Go fast
            VelocityUpOnPressedKey();
            // Jump
            Jump();
            // Update powerup position
            PowerfulIndicatorEmplacement();

            if(transform.position.y < LevelsComposants.Instance.outPosY) {
                ariaRb.useGravity = false;
                transform.position = new Vector3(transform.position.x, LevelsComposants.Instance.outPosY, transform.position.y);
                transform.Rotate(new Vector3(0,0,0));
                MainManager.Instance.GameOver();
            }
        }
    }

    // Strike & Velocity powerful indicator emplacement
    void PowerfulIndicatorEmplacement(){
        Vector3 offset = new Vector3(ariaRb.gameObject.transform.position.x, 
        ariaRb.gameObject.transform.position.y + 1.0f, 
        ariaRb.gameObject.transform.position.z);
        Vector3 powerupPosition = offset;
        strikePowerfulIndicator.transform.position = powerupPosition;
    }

    // When you press space button
    void VelocityUpOnPressedKey() {
        if (Input.GetKey(KeyCode.Space))
        {
            ariaRb.AddForce(focalPoint.transform.forward * velocityOnOrderForce * Time.deltaTime);
        }
    }

    // When player want to jump
    void Jump(){
        if(Input.GetKeyDown(KeyCode.LeftShift) || Input.GetKeyDown(KeyCode.RightShift)) {
            MainManager.Instance.PlayAudioClip(MainManager.Instance.jumpSound);
            ariaRb.AddForce(focalPoint.transform.up * jumpForce, ForceMode.Impulse);
            MainManager.Instance.InstantiateAlertText("+ Jump");
        }
    }

    void OnCollisionEnter(Collision collision) {
        // With strike force
        if(collision.gameObject.CompareTag(Tags.Instance.strikePowerful)) {
            MainManager.Instance.PlayAudioClip(MainManager.Instance.strikePowerfulSound);
            // Update strike powerful status
            hasStrikePowerful = true;
            // Destroy collider object
            collision.gameObject.SetActive(false);
            // Active Velocity powerful indicator indicator
            strikePowerfulIndicator.SetActive(true);   
            // Start coroutine
            StartCoroutine(StrikePowerfulRoutine(collision.gameObject));
        }

        // With item
        if(collision.gameObject.CompareTag(Tags.Instance.item)) {
            MainManager.Instance.PlayAudioClip(MainManager.Instance.itemSound);
            // Increment item counter
            LevelsComposants.Instance.itemsCount = LevelsComposants.Instance.itemsCount + 1;
            MainManager.Instance.InstantiateAlertText(LevelsComposants.Instance.itemsCount + " Item");
            // Destroy item object
            Destroy(collision.gameObject);
        }

        // With enemy
        if(collision.gameObject.CompareTag(Tags.Instance.enemy)) {
            MainManager.Instance.PlayAudioClip(MainManager.Instance.enemyGeneratorSound);
            // We're taking the distance between player & enemy
            Rigidbody enemyRigidbody = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 awayFromPlayer = collision.gameObject.transform.position - transform.position;
            
            if (hasStrikePowerful) {
                enemyRigidbody.AddForce(awayFromPlayer * strikePowerfulFactor, ForceMode.Impulse);
            } else {
                enemyRigidbody.AddForce(awayFromPlayer * LevelsComposants.Instance.powerupStrength * Time.deltaTime, ForceMode.Impulse);
            }
        }
    }

    void OnTriggerEnter(Collider collider) {
        if(collider.gameObject.CompareTag(Tags.Instance.teleporter)) {
            if(MainManager.Instance.canOpenTeleporter){
                MainManager.Instance.PlayAudioClip(MainManager.Instance.enemyGeneratorSound);
                MainManager.Instance.NextLevel();
            }
        }

        if(collider.gameObject.CompareTag(Tags.Instance.islandBound)) {
            MainManager.Instance.PlayAudioClip(MainManager.Instance.clashSound);
        }

        if(collider.gameObject.CompareTag(Tags.Instance.enemyGenerator)) {
            MainManager.Instance.PlayAudioClip(MainManager.Instance.enemyGeneratorSound);
            int random = Random.Range(1, 5);
            for (int i = 0; i < random; i++) {
                Instantiate(enemyPrefab, transform.position, enemyPrefab.transform.rotation);
            }
        }
    }

    IEnumerator StrikePowerfulRoutine(GameObject obj) {
        yield return new WaitForSeconds(strikePowerfulTime);
        hasStrikePowerful = false;
        strikePowerfulIndicator.SetActive(false);
        obj.SetActive(true);
    }

    void AriaOrientation() {
        // Update aria orientation from focal point orientation
        ariaRb.AddForce(focalPoint.transform.forward  * speedForce * verticalInput * Time.deltaTime);
        ariaRb.AddForce(Vector3.right  * speedForce * horizontalInput * Time.deltaTime);
    }
}
