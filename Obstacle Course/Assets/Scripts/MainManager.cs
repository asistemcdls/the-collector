using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using TMPro;
using System.IO;
#if UNITY_EDITOR
    using UnityEditor;
#endif

public class MainManager : MonoBehaviour
{
    public static MainManager Instance {get; private set;}
    public TextMeshProUGUI alertText;
    public GameObject alertTextPrefab;
    public TextMeshProUGUI levelText;
    public TextMeshProUGUI menuLevelText;
    public TextMeshProUGUI itemCounterText;
    public TextMeshProUGUI partTimerText;
    public TextMeshProUGUI recordText;
    public TextMeshProUGUI menuRecordText;
    public TextMeshProUGUI infoText;
    public GameObject teleporterPrefab;
    private GameObject teleporter;
    private GameObject ground;
    private List<GameObject> blocs;
    private Vector3 groundSize;
    private int totalItemsCount = 0;
    private bool levelStart = false;
    private bool pause = false;
    private bool teleporterStatus = false;
    private bool gameOver = false;
    public bool canOpenTeleporter = false;
    private System.TimeSpan timeSpent;

    public Canvas canvas;
    public GameObject menuUi;
    public GameObject missionUi;
    public GameObject missionScrollViewUi;
    public GameObject alertViewUi;
    [SerializeField] private GameObject missionItemPrefab;
    private GameObject topUi;
    private GameObject bottomUi;
    private GameObject infoUi;

    public Player player;

    private int countInfosSkip = 0; 

    private AudioSource playerAudio;
    private AudioSource playerAudioBack;
    public AudioClip clashSound;
    public AudioClip winSound;
    public AudioClip defeatSound;
    public AudioClip strikePowerfulSound;
    public AudioClip itemSound;
    public AudioClip jumpSound;
    public AudioClip menuSound;
    public AudioClip gameSound;
    public AudioClip enemyGeneratorSound;
    public AudioClip activeTeleporterSound;

    private void Awake(){
        if(Instance != null) {
            Destroy(gameObject);
            return;
        }
        Instance = this;
        DontDestroyOnLoad(gameObject);
        DontDestroyOnLoad(canvas);

        LoadDatas();

        LevelsComposants.Instance.GetLevelScene(LevelsComposants.Instance.level);
    }

    void CanvasPanelsInit(){
        topUi = FindContent(canvas.gameObject, "Top").gameObject;
        bottomUi = FindContent(canvas.gameObject, "Bottom").gameObject;
        infoUi = FindContent(canvas.gameObject, "Info").gameObject;

        topUi.SetActive(false);
        bottomUi.SetActive(false);
        infoUi.SetActive(false);
        menuUi.SetActive(true);
        missionUi.SetActive(false);
    }

    // Start is called before the first frame update
    void Start() {
        playerAudio = GetComponent<AudioSource>();

        CanvasPanelsInit();

        InitTeleporter();

        InitTextComposant();
        
        PlayAudioClip(gameSound, true);
        StartCoroutine(GameMusicCountdown());
    }

    // Update is called once per frame
    void Update() {
        if(LevelsComposants.Instance.level != 0) {
            InstantiateTeleporter();
            UpdateItemCountText();
            if(levelStart && LevelsComposants.Instance.level == 1 && countInfosSkip < LevelsComposants.Instance.GetInfos().Count){
                infoUi.SetActive(true);
                IntroInfos();
                if(countInfosSkip >= LevelsComposants.Instance.GetInfos().Count) {
                    infoUi.SetActive(false);
                }
            }
        } 

        // Pause / Play
        if(Input.GetKeyDown("enter") && menuUi.activeInHierarchy) {
            GameStart();
        } else  if(Input.GetKeyDown("enter") && !menuUi.activeInHierarchy) {
            pause = true;
            GoToMenu();
        }
        
        CanvasPanelsManagement();
    }

    public void InitTeleporter() {
        teleporterStatus = false;
        if(LevelsComposants.Instance.level != 0) {
            totalItemsCount = LevelsComposants.Instance.GetLevelItemCount(LevelsComposants.Instance.level);
        }
    }

    private void InitTextComposant() {
        alertText.SetText("");
        if(LevelsComposants.Instance.level == 0){
            levelText.gameObject.SetActive(false);
            menuRecordText.gameObject.SetActive(false);
            recordText.gameObject.SetActive(false);
            menuLevelText.SetText("Démarrer");
            var missionButtonUi = FindContent(canvas.gameObject, "MissionButton").gameObject;
            missionButtonUi.SetActive(false);
        } else {
            levelText.SetText("Lv." + LevelsComposants.Instance.level);
            menuLevelText.SetText("Lv." + LevelsComposants.Instance.level);
            string bestScore = "Record " + System.TimeSpan.FromMinutes(player.score).ToString(@"hh\:mm");
            menuRecordText.SetText(bestScore);
        }
    }
    void IntroInfos() {
        infoText.SetText(LevelsComposants.Instance.GetInfos()[countInfosSkip]);
        if(Input.GetKeyDown(KeyCode.Tab)) {
            countInfosSkip++;
            if(countInfosSkip < LevelsComposants.Instance.GetInfos().Count) {
                infoText.SetText(LevelsComposants.Instance.GetInfos()[countInfosSkip]);
            }
        }  
    }

    public void PlayAudioClip(AudioClip clip, bool loop = false){
        playerAudio.PlayOneShot(clip);
        playerAudio.loop = true;
    }

    public bool CanPlay() {
        return !pause && levelStart && !infoUi.activeInHierarchy;
    }

    void CanvasPanelsManagement() {
        if(levelStart) {
            topUi.SetActive(true);
            bottomUi.SetActive(true);
        }
    }

    public RectTransform FindContent(GameObject viewObject, string targetName) {
        RectTransform rectVal = null;
        Transform[] temp = viewObject.GetComponentsInChildren<Transform>();
        foreach (Transform Child in temp)
        {
            if(Child.name == targetName) {
                rectVal = Child.gameObject.GetComponent<RectTransform>();
            }
        }
        return rectVal;
    }

    public void GoToMenu() {
        pause = true;
        PlayAudioClip(menuSound);
        menuUi.SetActive(true);
        missionUi.SetActive(false);
        infoUi.SetActive(false);
        gameOver = false;
    }

    public void GoToMission() {
        PlayAudioClip(menuSound);
        menuUi.SetActive(false);
        missionUi.SetActive(true);
        infoUi.SetActive(false);
    }


    // When player check all items, we can display the teleporter
    void InstantiateTeleporter() {
        if (totalItemsCount == LevelsComposants.Instance.itemsCount) {
            // Instantiate or active the teleporter
            if(!teleporterStatus) {
                InstantiateAlertText("Teleporter ready...", 5);
            }
            canOpenTeleporter = true;
            PlayAudioClip(activeTeleporterSound);
            teleporterStatus = true;
        }else{
            canOpenTeleporter = false;
        }
    }

    public void InstantiateAlertText(string text, int second = 3) {
        var item_go = Instantiate(alertTextPrefab);   
            item_go.GetComponentInChildren<TMP_Text>().SetText(text);
            RectTransform parent = FindContent(alertViewUi, "AlertPanel");
            item_go.transform.SetParent(parent);
            item_go.transform.localScale = Vector2.one;
        StartCoroutine(AlertTextCoroutine(item_go , second));
    }

    public void UpdateItemCountText() {
        itemCounterText.SetText(LevelsComposants.Instance.itemsCount + "/" + totalItemsCount
         + " items");
    }

    public void NextLevel() {
        int newLevel = player.level;
        newLevel++;
        SaveData();
        LevelsComposants.Instance.UpdateLevel(newLevel);
        LevelsComposants.Instance.GetLevelScene(LevelsComposants.Instance.level);
        InitTeleporter();
        infoUi.SetActive(false);
        LevelsComposants.Instance.itemsCount = 0;
        InitTextComposant();
        topUi.SetActive(false);
        bottomUi.SetActive(false);
        levelStart = false;
        gameOver = false;
        GoToMenu();
    }

    private void RestartGame() {
        infoUi.SetActive(false);
        topUi.SetActive(false);
        bottomUi.SetActive(false);
        levelStart = false;
        gameOver = false;
        teleporterStatus = false;
        LevelsComposants.Instance.itemsCount = 0;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        InitTeleporter();
        GoToMenu();
    }

    public void GameStart() {
        gameOver = false;
        menuUi.SetActive(false);
        missionUi.SetActive(false);
        pause = false;
        StartCoroutine(StartCountdown(LevelsComposants.Instance.levelReadyTimer));
    }

    public void GameOver() {
        Debug.Log("Game Over...");
        if(!gameOver) {
            InstantiateAlertText("Game Over !!!");
            gameOver = true;   
        }
        
        RestartGame();
    }

    IEnumerator AlertCoroutine(float second = 3) {
        yield return new WaitForSecondsRealtime(second);
        alertText.SetText("");
        alertText.StopAllCoroutines();
    }

    IEnumerator AlertTextCoroutine(GameObject obj, float second = 3) {
        yield return new WaitForSecondsRealtime(second);
        Destroy(obj);
    }

    private IEnumerator StartCountdown(float duration) {
        float counter = duration;
        while (counter > 0)
        {
            yield return new WaitForSeconds(1);
            alertText.SetText("" + counter.ToString());
            counter -= 1;
        }
        alertText.SetText("Go !!!");
        StartCoroutine(AlertCoroutine());
        levelStart = true;
        StartCoroutine(GamePartCountdown(LevelsComposants.Instance.GetLevelTimeCount(LevelsComposants.Instance.level)));
    }

    private IEnumerator GamePartCountdown(float duration) {
        float counter = duration * 60;
        while (counter > 0 && levelStart)
        {
            yield return new WaitForSeconds(1);
            if(!pause) {
                timeSpent = System.TimeSpan.FromMinutes(counter);
                partTimerText.SetText(timeSpent.ToString(@"hh\:mm"));
                counter -= 1;
            }
        }
        if(counter == 0) {
            GameOver();
        }
    }

    private IEnumerator GameMusicCountdown() {
        while (transform.gameObject.activeInHierarchy)
        {
            yield return new WaitForSeconds(30);
            PlayAudioClip(gameSound);
        }
    }

    public void Exit() { 
        #if UNITY_EDITOR
            EditorApplication.ExitPlaymode();
        #else
            Application.Quit();
        #endif
    }

    public void SaveData() {
        Player newPlayer = new Player();
        float time = (LevelsComposants.Instance.GetLevelTimeCount(LevelsComposants.Instance.level) * 60)
                     - ((float)timeSpent.TotalMinutes);
        
        if ((time < player.score) && player.score != 0) {
            newPlayer.score = time;
        } else if(player.score == 0) {
            newPlayer.score = time;
        }
        newPlayer.level = LevelsComposants.Instance.level;
        player.level = newPlayer.level;
        player.score = newPlayer.score;
        string json = JsonUtility.ToJson(newPlayer);
        File.WriteAllText(Application.persistentDataPath + "/collector_game" + ".json", json);
    }

    public void LoadDatas() {
        string path = Application.persistentDataPath +"/collector_game"  + ".json";
        if (File.Exists(path)){
            string json = File.ReadAllText(path);
            player = JsonUtility.FromJson<Player>(json);
            LevelsComposants.Instance.UpdateLevel(player.level);
        }else{
            player = new Player();
            LevelsComposants.Instance.UpdateLevel(1);
            player.level = LevelsComposants.Instance.level;
            player.score = 0;
        }
    }
}
