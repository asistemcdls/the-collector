using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCamera : MonoBehaviour
{
    [SerializeField] private float rotateSpeed = 50.0f;
    private float horizontalInput = 0.0f;
    private GameObject player;
    private GameObject cam;
    private Vector3 focalPosition;
    private Vector3 offset;
    private float distance;

    void Start(){
        cam = GameObject.FindGameObjectWithTag(Tags.Instance.mainCamera);
        player = GameObject.FindWithTag(Tags.Instance.player);
        distance = Vector3.Distance(player.transform.position, cam.transform.position);
        offset = player.transform.position - cam.transform.position;
    }
    // Update is called once per frame
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        RotateWithAxis();
    }

    void RotateWithAxis() {
        if(Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftArrow)){
            transform.RotateAround( player.transform.position,Vector3.down, Time.deltaTime * rotateSpeed);
        }

        if(Input.GetKey(KeyCode.LeftControl) && Input.GetKey(KeyCode.RightArrow)){
            transform.RotateAround( player.transform.position,Vector3.up, Time.deltaTime * rotateSpeed);
        }
        
        Vector3 d = player.transform.position - transform.position;
        transform.position = new Vector3(player.transform.position.x - transform.rotation.x , 
            (player.transform.position.y + 10.0f ) + transform.rotation.y, (player.transform.position.z  - 25.0f) + transform.rotation.z) ;
    }

    void RotateAlways() {
        transform.Rotate(Vector3.up * Time.deltaTime * rotateSpeed);
    }
}
