using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelsComposants : MonoBehaviour
{

    public static LevelsComposants Instance { get; private set; }
    // Start is called before the first frame update
    private void Awake() {
        // start of new code
        if(Instance != null){
            Destroy(gameObject);
            return;
        }   
        // end of new code
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public int level {get; private set;}
    public float enemyPressureDistance = 10.0f;
    public float powerupStrength = 100.0f;
    public float outPosY = - 15.0f;

    public float levelReadyTimer = 5.0f;
    [SerializeField] public int itemsCount = 0;
    private float levelOneTimeCounter = 5.0f;
    private float levelTwoTimeCounter = 5.0f;
    private float levelThreeTimeCounter = 7.0f;
    private float levelFourTimeCounter = 7.0f;
    private float levelFiveTimeCounter = 10.0f;

    public void UpdateLevel(int v){
        level = v;
    }

    public float GetLevelTimeCount(int niv) {
        float counter;
        switch (niv){
            case 1:
                counter = levelOneTimeCounter;
            break;
            case 2:
                counter = levelTwoTimeCounter;
            break;
            case 3:
                counter = levelThreeTimeCounter;
            break;
            case 4:
                counter = levelFourTimeCounter;
            break;
            case 5:
                counter = levelFiveTimeCounter;
            break;
            default:
                counter = levelOneTimeCounter;
            break;
        }
        return counter;
    }

    public int GetLevelItemCount(int nv) {
        int counter;
        switch (nv){
            case 1:
                counter = 3;
            break;
            case 2:
                counter = 3;
            break;
            case 3:
                counter = 3;
            break;
            case 4:
                counter = 3;
            break;
            default:
                counter = 3;
            break;
        }
        return counter;
    }

    public void GetLevelScene(int lv) {
        SceneManager.LoadScene(lv);
    }

    public List<string> GetLevelMissions(int actualLevel = 1, int totalItems = 0, float seconds = 0){
        List<string> mission  = new List<string>();
        mission.Add("Niveau actuel de jeu : " + actualLevel + ". Correspondance de débutant.");
        mission.Add("Récupérer " + totalItems + " items.");
        mission.Add("Temps de la partie : " + seconds + " secondes.");
        return mission;
    }

    public List<string> GetInfos() {
        List<string> infos  = new List<string>();
        infos.Add("Vous avez pour mission de récupérer les items en forme de cube jaune pour activer, "+
                "un ``téléporteur (Portail de téléportation)`` .");
        infos.Add("Appuyer ``ESPACE`` pour accélerer.");
        infos.Add("Appuyer ``SHIFT`` pour sauter.");
        infos.Add("Appuyer ``Haut``, ``Bas``, ``Gauche``, ``Droite`` pour rouler vers l'avant et les côtés.");
        infos.Add("Appuyer ``CTRL + direction`` (GAUCHE ou DROITE) pour tourner dans le sens correspondant.");
        infos.Add("Les Bonus jaune ``Strike`` vous donne beaucoup plus de puissance de frappe.");
        infos.Add("Pour passer le niveau, il faut traverser un téléporteur.");
        infos.Add("Les enemis vous empêchent d'avancer quand voud être proche d'un ``ITEM`` ou d'un ``TELEPORTEUR``.");
        infos.Add("Les grosses boules rouge génèrent des ``Arias`` (enemis) au contact.");
        return infos;
    }
}
