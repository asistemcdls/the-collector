
using UnityEngine;

public class Tags : MonoBehaviour
{
    public static Tags Instance { get; private set; }
    
    private void Awake(){
        // start of new code
        if(Instance != null){
            Destroy(gameObject);
            return;
        }   
        // end of new code
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
    // Tags statics variables
    public string focalPoint  = "FocalPoint";
    public string mainCamera  = "MainCamera";
    public string velocityPowerful = "VelocityPowerful";
    public string strikePowerful = "StrikePowerful";
    public string item = "Item";
    public string player = "Player";
    public string ground = "Ground";
    public string bloc = "Bloc";
    public string enemy = "Enemy";
    public string teleporter = "Teleporter";
    public string islandBound = "IslandBound";
    public string enemyGenerator = "EnemyGenerator";
}
