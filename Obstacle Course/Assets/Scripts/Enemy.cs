using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private float speedForce = 30.0f;
    private Vector3 originPosition;
    private Rigidbody enemyRb;
    private GameObject player;
    // Start is called before the first frame update
    void Start() {
        enemyRb = GetComponent<Rigidbody>();
        player = GameObject.FindGameObjectWithTag(Tags.Instance.player);
        originPosition = transform.position;
    }

    // Update is called once per frame
    void Update(){
        if(MainManager.Instance.CanPlay()){
            ForwardPlayer();
        }
    }

    void ForwardPlayer(){
        Vector3 lookDirection = (player.transform.position - transform.position).normalized;
        Vector3 backDirection = (originPosition - transform.position).normalized;
        float distance = Vector3.Distance(player.transform.position, transform.position);
        // Si la distance est inférieur une certaine unité de distance peu importe , on attaque le joueur
        if(distance < LevelsComposants.Instance.enemyPressureDistance){
            enemyRb.AddForce(lookDirection * speedForce);
        } else {
            // Sinon, l'enemy retourne vers sa garde (position originel)
            enemyRb.AddForce(backDirection * speedForce);
        }
        
        if(transform.position.y < LevelsComposants.Instance.outPosY){
            Destroy(enemyRb.gameObject);
        }
    }
}
