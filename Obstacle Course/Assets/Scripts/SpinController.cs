using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpinController : MonoBehaviour
{
    private List<Quaternion> startRotations = new List<Quaternion>();
    private List<GameObject> objs = new List<GameObject>();
    void Start() {
        foreach (var item in GameObject.FindGameObjectsWithTag(Tags.Instance.strikePowerful)) {
            objs.Add(item);
            startRotations.Add(item.transform.localRotation);
        }
    }
    // Update is called once per frame
    void Update() {
    }

    private void RotateObj(GameObject obj, Quaternion q) {
        obj.transform.Rotate(new Vector3(0f,100.0f,0f) * Time.deltaTime);
    }
}
